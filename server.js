'use strict';
const express = require('express');
const path = require('path');
const logger = require('morgan');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const cors = require('cors');
const passport = require('passport');
const passportMiddleware = require('./auth');
const apiRoutes = require('./routes');

const app = express();

const corsOptions = {
  origin: 'https://localhost:4200',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

const staticAngularDirPath = express.static(path.resolve(__dirname, process.env.STATIC_NG_BUILD_DIR_PATH));
const staticPublicAssets = express.static(path.resolve(__dirname, process.env.STATIC_PUBLIC_ASSETS))

app.use(helmet());
app.use(cors(corsOptions));
// ENABLE DEV-LOGGING
app.use(logger('dev'));

// Include Passport middleware
passportMiddleware(passport);

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

app.use(staticAngularDirPath);
app.use(staticPublicAssets);

// ROUTES START------------------------------------------------------------
app.use('/', staticAngularDirPath);
// API v1 Routes
app.use('/api/v1', apiRoutes);
// If no route will match then server default route
app.get('*', async (req, res) => {
  res.status(200).send({ message: `Welcome to MEAN STACK Running -- ${process.env.NODE_ENV} enviroment` });
});

//ROUTE ENDS----------------------------------------------------------------

// Catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// Global error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500).send(process.env.NODE_ENV !== 'production' ? { error: err.message || err.stack } : {error: err.message || 'Somthing went wrong!! Please contact site admin'});
});

module.exports = app;
