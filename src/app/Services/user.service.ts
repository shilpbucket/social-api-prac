import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SocialUser } from 'angularx-social-login';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

export interface AuthTokenRes {
  status: string;
  message: string;
  data: AuthToken;
}

interface AuthToken {
  token: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  readonly env = environment;
  constructor(private http: HttpClient) { }


  issueAuthToken(user: SocialUser): Observable<AuthTokenRes> {
    const URL = `${this.env.DEV_API_SERVER}/user/oAuth/login`;
    return this.http.post<AuthTokenRes>(URL, user);
  }


  setLocalStorage(key: string, value: string): void {
    return localStorage.setItem(key, JSON.stringify(value));
  }

  getLocalStorage(key: string): string {
    return JSON.parse(localStorage.getItem(key));
  }
}
