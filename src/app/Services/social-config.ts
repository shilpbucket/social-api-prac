import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider } from 'angularx-social-login';
import {environment} from '../../environments/environment';

const env = environment;

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider(env.google_client_id)
  }
]);

const provideConfig = () => {
  return config;
};

export { SocialLoginModule, AuthServiceConfig, provideConfig };

