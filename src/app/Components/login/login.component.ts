import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService, AuthTokenRes } from '../../Services/user.service';
import {
  AuthService,
  SocialUser,
  GoogleLoginProvider,
} from 'angularx-social-login';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  signinForm: FormGroup;
  user: SocialUser;
  loggedIn: boolean;
  userSubscription: Subscription;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private userService: UserService
  ) {
    this.userSubscription = this.authService.authState.subscribe((user: SocialUser) => {
      this.user = user;
      this.loggedIn = user != null;
      console.log(this.user);
      if (this.user) {
        this.issueAuthToken(user);
      }
    });
  }

  ngOnInit(): void {
    this.signinForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }
  signOut(): void {
    this.authService.signOut();
  }

  issueAuthToken(user: SocialUser) {
    this.userService.issueAuthToken(user).subscribe((res: AuthTokenRes) => {
      if (res.status === 'success') {
        const { data } = res;
        this.userService.setLocalStorage('token', data.token);
        this.router.navigate(['/profile']);
      }
    });
  }
}
