import { Component, OnInit, OnDestroy } from '@angular/core';
import { SocialUser, AuthService } from 'angularx-social-login';
import { UserService } from 'src/app/Services/user.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit, OnDestroy {

  user: SocialUser;
  userSubscription: Subscription;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private router: Router
  ) {
    this.userSubscription = this.authService.authState.subscribe((user) => (this.user = user));
  }

  ngOnInit(): void { }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  signOut() {
    this.authService.signOut().then(success => {
      localStorage.removeItem('token');
      this.router.navigate(['/login']);
    });
  }
}
