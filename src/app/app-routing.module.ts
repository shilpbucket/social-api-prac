import { NgModule, Injectable } from '@angular/core';
import { Routes, RouterModule, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { LoginComponent } from './Components/login/login.component';
import { ProfileComponent } from './Components/profile/profile.component';
import { UserService } from './Services/user.service';
import { Observable } from 'rxjs';


@Injectable()
class CanActivateProfile implements CanActivate {
  constructor(private router: Router, private userService: UserService) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (!this.userService.getLocalStorage('token')) {
      return this.router.parseUrl('login');
    }
    return true;
  }
}
const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'profile', canActivate: [CanActivateProfile], component: ProfileComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  providers: [CanActivateProfile],
  exports: [RouterModule]
})
export class AppRoutingModule { }
