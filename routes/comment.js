const express = require('express');
const commentRouter = express.Router({ mergeParams: true });
const { Comment, Status } = require("../models/mongoose");

/* GET ALL COMMENTS BY ID */
commentRouter.get('/:commentId', function (req, res, next) {
    const commentId = req.params.commentId;
    Comment.findById(commentId).exec((err, comment) => {
        if (err) return next(err);
        res.status(!comment ? 204 : 200).send({ status: 'success', data: comment });
    });
});

/* CREATE NEW USER STATUS */
commentRouter.post('/', (req, res, next) => {

    const sql_user_ref = req.params.id || res.body.sql_user_ref;
    const status = req.params.statusId || res.body.status;
    // return res.json({ ...req.body, sql_user_ref, status });
    Comment.create({ ...req.body, sql_user_ref, status }, async (err, comment) => {
        if (err) return next(err);
        try {
            await Status.findOneAndUpdate({ _id: status }, { $push: { commentsIds: comment._id } });
        } catch (err) {
            if (err) return next(err);
        }
        res.send({ status: 'success', message: 'successfully created comment', data: comment });
    });
});

/* UPDATE Comment */
commentRouter.put('/:commentId', function (req, res, next) {
    Comment.findByIdAndUpdate(req.params.commentId, req.body, {
        upsert: true
    }).exec((err, comment) => {
        if (err) return next(err);
        res.status(!comment ? 204 : 200).send({ status: 'success', message: 'successfully updated a comment', data: comment });
    });
});

/* DELETE USER */
commentRouter.delete('/:commentId', function (req, res, next) {
    Comment.findByIdAndRemove(req.params.commentId).exec((err, comment) => {
        if (err) return next(err);
        res.json({ status: 'success', message: 'successfully deleted a comment' });
    });
});

module.exports = commentRouter;