const express = require('express');
const PostStatusRouter = express.Router({ mergeParams: true });
const commentRouter = require('./comment');
const { Status, Comment } = require("../models/mongoose");

/* GET ALL STATUES OF USER BY ID */
PostStatusRouter.get('/', (req, res, next) => {
    const sql_user_ref = req.params.id;
    Status.find({ sql_user_ref })
        .populate({ path: 'commentsIds', select: ['username', 'comment', 'created_at'] })
        .exec((err, statuses) => {
            if (err) return next(err);
            res.status(!statuses ? 204 : 200).send({ status: 'success', data: statuses });
        });
});

/** STORE STATUS LIKES */
PostStatusRouter.post('/:statusId/like', (req, res, next) => {
    const like = req.body.like || false;
    const lCounter = !like ? -1 : 1;
    // Reset counter if likes droppes to 0
    Status.findOneAndUpdate({ _id: req.params.statusId, likes: { $lt: 0 } }, { $set: { likes: 0 } }).exec((err, affectedLikes) => {
        if (err) return next(err);
    })
    Status.findOneAndUpdate({ _id: req.params.statusId }, { $inc: { likes: lCounter } })
        .exec((err, affectedLikes) => {
            if (err) return next(err);
            res.status(200).send({ status: 'success' });
        });
});

/* GET SINGLE STATUS BY ID */
PostStatusRouter.get('/:statusId', (req, res, next) => {
    Status.findById(req.params.statusId).exec((err, status) => {
        if (err) return next(err);
        res.status(!status ? 204 : 200).send({ status: 'success', data: status });
    });
});

/* CREATE NEW USER STATUS */
PostStatusRouter.post('/', (req, res, next) => {
    const sql_user_ref = req.params.id || res.body.sql_user_ref;
    Status.create({ ...req.body, sql_user_ref }, (err, status) => {
        if (err) return next(err);
        res.status(!status ? 204 : 200).send({ status: 'success', message: 'successfully posted a status', data: status });
    });
});

/* UPDATE USER STATUS */
PostStatusRouter.put('/:statusId', (req, res, next) => {
    Status.findByIdAndUpdate(req.params.statusId, req.body, {
        new: true
    }).exec((err, status) => {
        if (err) return next(err);
        res.status(!status ? 204 : 200).send({ status: 'success', message: 'successfully updated a status', data: status });
    });
});
/* DELETE USER STATUS*/
PostStatusRouter.delete('/:statusId', (req, res, next) => {
    Status.findByIdAndRemove(req.params.statusId).exec((err, status) => {
        if (err) return next(err);
        res.json({ status: 'success', message: 'successfully deleted a status' });
    });
});

/**
 * Attach Comment Routes
 */
PostStatusRouter.use('/:statusId/comment', commentRouter);

module.exports = PostStatusRouter;