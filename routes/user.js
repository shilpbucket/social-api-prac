const express = require('express');
const router = express.Router();
const ProfileRouter = UserFriendRouter = express.Router({mergeParams: true});
const Multer = require('../config/multer');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const { User, UserFriend } = require("../controllers");
const PostStatusRouter = require("./status");
const ensureAuthProtected = passport.authenticate('jwt', { session: false });

router.post('/login', async (req, res, next) => {
  passport.authenticate('login', async (err, user, info) => {
    return User.issueToken({ err, user }, req, res, next);
  })(req, res, next);
});

router.get('/logout', async (req, res, next) => {
  req.logOut();
});

router.post('/oAuth/login', async (req, res, next) => {
  passport.authenticate('oAuthGoogleToken', async (err, user, info) => {
    return User.issueToken({ err, user }, req, res, next);
  })(req, res, next);
});

router.post('/signup', passport.authenticate('signup', { session: false }), async (req, res, next) => {
  res.json({
    status: 'success',
    message: 'Signup successful',
    user: req.user
  });
});

ProfileRouter.get('/', User.getProfile);
ProfileRouter.put('/update', User.updateProfile);
ProfileRouter.delete('/delete', User.delete);
ProfileRouter.post('/upload', Multer.single('profilePic'), User.updateProfilePic);
ProfileRouter.get('/picture', User.getProfilePic);

UserFriendRouter.get('/', (req, res, next) => {
  res.send('GET User friends list');
});

UserFriendRouter.post('/', UserFriend.addFriend);

router.get('/', ensureAuthProtected, User.findAll);
// Secure user profile routes
router.use('/:id/profile', ensureAuthProtected, ProfileRouter);
// Secure user friends routes
router.use('/:id/friend', ensureAuthProtected, UserFriendRouter);
// Secure user status routes
router.use('/:id/status', ensureAuthProtected, PostStatusRouter);

module.exports = router;
