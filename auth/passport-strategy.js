'use strict';
const passport = require('passport');
const ExtractJwt = require("passport-jwt").ExtractJwt;
const localStrategy = require('passport-local').Strategy;
const CustomStrategy = require('passport-custom').Strategy;
const JWTstrategy = require('passport-jwt').Strategy;
const dbModel = require('../models');
const { User } = dbModel;
/**
 * JWT Custom strategy
 */
module.exports = {
    oAuthGoogleTokenStrategy: new CustomStrategy(async (payload, done) => {
        // Finding the user in the database using payload body using token
        // console.log('Req payload inside JWT custom oAuth stretegy', payload);
        const { idToken, email, name } = payload.body;
        try {
            // If User already exists return else create it
            const [user, created] = await User.findOrCreate({
                where: { oAuthIdTokenGoogle: idToken },
                defaults: { email, name, oAuthIdTokenGoogle: idToken }
            });
            // Get Plain Object of user
            const leanUser = user.get({ plain: true });
            return done(null, leanUser);
            // if (!user) return done(new Error("User not found"), null);
        } catch (err) {
            console.log(err, 'Something wrong happen while finding user by token');
            return done(err);
        }
    }),
    // Create a passport middleware to handle user registration
    signup: new localStrategy({
        usernameField: 'email',
        passwordField: 'password'
    }, async (email, password, done) => {
        try {
            // Save the information provided by the user to the the database
            const user = await User.create({ email, password });
            // Send the user information to the next middleware
            return done(null, user);
        } catch (error) {
            done(error);
        }
    }),
    // Create a passport middleware to handle User login
    login: new localStrategy({
        usernameField: 'email',
        passwordField: 'password'
    }, async (email, password, done) => {
        try {
            // Find the user associated with the email provided by the user
            const user = await User.findOne({ where: { email } });
            // console.log(user, 'User found');
            if (!user) {
                //If the user isn't found in the database, return a message
                return done(null, false, { message: 'User not found' });
            }
            // Validate password and make sure it matches with the corresponding hash stored in the database
            // If the passwords match, it returns a value of true.
            const validate = await user.isValidPassword(password);
            // console.log('Passoword', validate);
            if (!validate) {
                return done(null, false, { message: 'Wrong Password' });
            }
            const userObj = user.get({ plain: true });
            console.log(userObj);
            //Send the user information to the next middleware
            return done(null, userObj, { message: 'Logged in Successfully' });
        } catch (error) {
            return done(error);
        }
    }),
    // Create an middleware to allow token access for routes
    validateToken: new JWTstrategy({
        // Secret we used to sign our JWT
        secretOrKey: process.env.JWT_SECRET || 'top_secret',
        //we expect the user to send the token as a query parameter with the name 'secret_token'
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
    }, async (token, done) => {
        try {
            // Pass the user details to the next middleware
            return done(null, token.user);
        } catch (error) {
            done(error);
        }
    })
};
