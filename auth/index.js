'use strict';
const passportStrategy = require('./passport-strategy');

const { oAuthGoogleTokenStrategy, signup, login, validateToken } = passportStrategy;


module.exports = passport => {
    passport.use('login', login);
    passport.use('signup', signup);
    passport.use('oAuthGoogleToken', oAuthGoogleTokenStrategy);
    passport.use(validateToken);
}

// module.exports = {
//     initialize: () => {
//         return passport.initialize();
//     },
//     authenticate: (req, res, next) => {
//         return passport.authenticate("jwt", {
//             session: false
//         }, (err, user, info) => {
//             if (err) {
//                 console.log(err);
//                 return next(err);
//             }
//             if (!user) {
//                 return res.json({
//                     status: 'error',
//                     error: 'ANOTHORIZED_USER'
//                 });
//             }
//             // Forward user information to the next middleware
//             req.user = user;
//             next();
//         })(req, res, next);
//     }
// };