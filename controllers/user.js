"use strict";
const dbModel = require("../models");

const jwt = require('jsonwebtoken');
const { User, UserFriends } = dbModel;
const Utils = require('./utils');

module.exports = {
    issueToken: async ({ err, user }, req, res, next) => {
        try {
            if (err || !user) {
                const error = new Error('An Error occurred')
                return next(error);
            }
            req.login(user, { session: false }, async (error) => {
                if (error) return next(error);
                // We don't want to store the sensitive information such as the
                // user password in the token so we pick only the email and id
                const body = { id: user.id, email: user.email };
                //Sign the JWT token and populate the payload with the user email and id
                const token = jwt.sign({ user: body }, process.env.JWT_SECRET || 'top_secret');
                //Send back the token to the user
                return res.json({ status: 'success', message: 'Login successful', data: { token } });
            });
        } catch (error) {
            return next(error);
        }
    },
    findAll: async (req, res) => {
        try {
            const users = await User.findAll();
            const mappedUsers = await Utils.generateProfileImageUrls(users);
            res.status(!users ? 204 : 200).send({ status: 'success', data: mappedUsers });
        } catch (err) {
            req.next(err);
        }
    },
    getProfile: async (req, res) => {
        try {
            const profile = await User.findByPk(req.params.id);
            res.status(!profile ? 204 : 200).send(profile);
        } catch (err) {
            req.next(err);
        }
    },
    updateProfile: async (req, res) => {
        const body = req.body;
        const id = req.params.id;
        try {
            const update = await User.update(
                {
                    email: body.email,
                    oAuthIdTokenGoogle: body.idToken || null,
                    name: body.name
                },
                { where: { id } }
            );
            res.status(200).send(`Successfully updated a user with id = ${id}`);
        } catch (err) {
            console.log("err while update user info", body);
            req.next(err);
        }
    },
    updateProfilePic: async (req, res) => {
        const id = req.params.id;
        try {
            await User.update({ profilePic: req.file.buffer }, { where: { id } });
            res.status(200).send({ status: 'success', message: `Profile picture uploaded successfully! -> filename = ' ${req.file.originalname}` });
        } catch (err) {
            console.log("err while update uploading profile pic", body);
            req.next(err);
        }
    },
    getProfilePic: async (req, res) => {
        const id = req.params.id;
        try {
            const user = await User.findByPk(req.params.id);
            const fileContents = Buffer.from(user.profilePic, "base64");
            res.set('Content-Type', 'image/jpeg');
            res.send(fileContents);
        } catch (err) {
            console.log("err while fetching profile pic");
            req.next(err);
        }
    },
    delete: async (req, res) => {
        const id = req.params.id;
        try {
            await User.destroy({ where: { id } });
            res.status(200).send(`Successfully delete a profile with id = ${id}`);
        } catch (err) {
            req.next(err);
        }
    }
};
