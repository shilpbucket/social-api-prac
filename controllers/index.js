'use strict';
const User = require('./user');
const UserFriend = require('./friend');

module.exports = { User, UserFriend };