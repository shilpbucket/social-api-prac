"use strict";
const { Sequelize, UserFriends } = require("../models");
// const { UserFriends } = dbModel;

module.exports = {
    addFriend: async (req, res) => {
        const userId = req.params.id;
        const body = req.body;
        console.log(body);
        try {
            const friend = await UserFriends.create({ UserId: userId, userFriendId: body.friendId});
            res.send({message: 'Friend request send'});
        } catch (err) {
            console.log("Friend request err", err);
            if (err instanceof Sequelize.ForeignKeyConstraintError) {
                req.next({status: 501, message: 'Invalid friend request', error: err.message});
              } else {
                req.next(err);
              }
           
        }
    }
};
