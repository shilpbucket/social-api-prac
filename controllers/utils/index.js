'use strict';
const fs = require("fs");
const path = require("path");

module.exports = {
    generateProfileImageUrls: async (users) => {
        const getProfileUrl = user => { // A function that returns a promise
            if (user.profilePic) {
                try {
                    const tempName = `${new Date().getTime()}_profile.jpeg`;
                    const filePath = path.join(__dirname, `../../`, `${process.env.STATIC_PUBLIC_ASSETS}/images/${tempName}`);

                    const bufferedImage = Buffer.from(user.profilePic, 'base64');
                    // Write to file into public images bucket
                    fs.writeFileSync(filePath, bufferedImage);
                    // Generate URL for profile access easily
                    user.profilePic = `${process.env.NODE_HOST}:${process.env.PORT}/images/${tempName}`;
                } catch (err) {
                    return Promise.reject(new Error(err));
                }
            }
            return Promise.resolve(user);
        }
        const asyncPromiseWrapper = async user => {
            return await getProfileUrl(user);
        }
        return Promise.all(users.map(user => asyncPromiseWrapper(user)));
    }
}
