## Social-API MEAN Stack  Angular--v9 

## Git Main Branch : develop
For clone ref: git clone https://shilpbucket@bitbucket.org/shilpbucket/social-api-prac.git 

## ENV SETUP
use cmd(Linux or Unix system) --- cp .env.exmple .env
or use cmd(Windows powershell) --- Copy-Item .\.env.example -Destination .env

## Public Dir SETUP
Please create a public/images directory directory under root folder if not created, or may else u will run into error while uploading images through APIs.

We are using this directory to server image assets uploaded by users.

## NODE Server Requirments >= 12.16.x

## HOW TO START Angular Development server over HTTPS PORT
Run npm run start:ng for a dev server. Navigate to https://localhost:4200/. The app will automatically reload if you change any of the source files.

## HOW TO START Node API server
Run npm run start:dev for a dev api server. Navigate to http://localhost:8080 (Your env settings affected here). The app will automatically reaload if you change any of the server files

## HOW TO BUILD FOR PRODUCTION
Change environment to production in .env file( key ref NODE_ENV=production)
Run npm run build:prod to generate angular prod ready build

## POSTMAN Collection
https://www.getpostman.com/collections/a8a5d67906fdf4966ee5


## Further help
To get more help on the Angular CLI use ng help or go check out the Angular CLI README.
Still needed help, reach out to me at er.shilp@gmail.com