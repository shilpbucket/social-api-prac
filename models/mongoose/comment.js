'use strict';
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const CommentSchema = new Schema({
    sql_user_ref: { type: Number, required: true },
    username: { type: String, required: true },
    status: { type: Schema.Types.ObjectId, ref: 'Status', required: true },
    comment: { type: String, required: true },
    created_at: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});

const Comment = mongoose.model('Comment', CommentSchema);

module.exports = Comment;