'use strict';
const Comment = require('./comment');
const Status = require('./status');

module.exports = { Comment, Status };