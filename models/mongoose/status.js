'use strict';
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const statusSchema = new Schema({
    author: { type: String, required: true },
    sql_user_ref: { type: Number, required: true },
    desc: { type: String, required: true },
    commentsIds: [{ type: Schema.Types.ObjectId, ref: 'Comment' }],
    likes: { type: Number, default: 0 },
    created_at: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});

const Status = mongoose.model('Status', statusSchema);
module.exports = Status;