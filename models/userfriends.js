'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserFriends = sequelize.define('UserFriends', {
    requestStatus: {type: DataTypes.ENUM('pending', 'accepted'), defaultValue: 'pending'}
  }, {});
  UserFriends.associate = function(models) {
     // associations can be defined here
  };
  return UserFriends;
};