"use strict";
const Sequelize = require("sequelize");
const dbConfig = require("../config/db_config");
const fs = require("fs");
const path = require("path");

const basename = path.basename(module.filename);

let db = {},
  sequelize;

if (dbConfig.url) {
  sequelize = new Sequelize(dbConfig.url, dbConfig);
} else {
  const { DB, USER, PASSWORD, HOST, dialect, pool } = dbConfig;
  sequelize = new Sequelize(DB, USER, PASSWORD, {
    HOST,
    dialect,
    pool,
  });
}

fs.readdirSync(__dirname)
  .filter((f) => {
    // Filter only files and current basemodule
    return f.indexOf(".") !== 0 && f !== basename && f.slice(-3) === ".js";
  })
  .forEach((file) => {
    const model = sequelize["import"](path.resolve(__dirname, file));
    db[model.name] = model;
  });

// Attach relationships between models if exists
Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
