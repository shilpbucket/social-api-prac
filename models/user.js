'use strict';
const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    oAuthIdTokenGoogle: DataTypes.STRING(1666),
    profilePic: DataTypes.BLOB
  }, {
    hooks: {
      'beforeCreate': async function (user, options) {
        // Replace the plain text password with the hash and then store it
        if (user.password) {
          try {
            const hashedPassword = await bcrypt.hash(user.password, 10);
            user.password = hashedPassword;
          } catch (err) {
            throw err;
          }
        }
      }
    }
  });

  User.associate = function (models) {
    // associations can be defined here
    User.belongsToMany(models.User, {as: 'userFriends', through: models.UserFriends});
  };
  // Attach classMethod for compare password
  User.prototype.isValidPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
  }

  return User;
};