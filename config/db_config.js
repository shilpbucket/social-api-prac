'use strict';
const dbConfig = {
  mysql: {
    development: {
      HOST: process.env.DB_HOST || "localhost",
      USER: process.env.DB_USER || "root",
      PASSWORD: process.env.DB_PASS || "",
      DB: process.env.DB_NAME || "sndk",
      dialect: process.env.DB_TYPE || "mysql",
      pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
      }
    },
    production: {
      HOST: "",
      USER: "",
      PASSWORD: "",
      DB: "",
      dialect: "mysql",
      pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
      }
    }
  },
  postgres: {
    development: {
      url: process.env.DEV_DB_URL,
      dialect: 'postgres',
    },
    test: {
      url: process.env.TEST_DB_URL,
      dialect: 'postgres',
    },
    production: {
      url: process.env.PROD_DB_URL,
      dialect: 'postgres',
    }
  }
};

const DB_TYPE = dbConfig[process.env.DB_TYPE];
const DB_CONFIG = DB_TYPE[process.env.NODE_ENV];

module.exports = DB_CONFIG;
